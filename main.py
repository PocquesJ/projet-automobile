import csv

def lecture(filepath, delimiter='|'):
    
    rows = []
    with open(filepath, 'r') as csvin:
        csvreader = csv.DictReader(csvin, delimiter=delimiter)
        for row in csvreader:
            rows.append(row)
    return rows

def changement_delimiteur(fichier):
    f = 0
    f=open(fichier,'r')         
    L=f.readlines()             
    for Ligne in L:            
        Ligne=Ligne.replace('|',';')    
    f.close()                  

def ecriture(filepath, rows, fieldnames, delimiter=';'):
    
    with open(filepath, 'w', newline='') as csvout:
        csvwriter = csv.DictWriter(
            csvout, fieldnames=fieldnames, delimiter=delimiter)
        csvwriter.writeheader()
        csvwriter.writerows(rows)


def changement_ligne(row, config):
   
    new_row = {}
    for key, attr in config.items():
        new_row[key] = row[attr]
    return new_row


def changement(rows, config):
    
    return [transform_row(row, config) for row in rows]
